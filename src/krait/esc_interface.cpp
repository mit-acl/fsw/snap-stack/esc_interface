/**
 * @file esc_interface.cpp
 * @brief ESC Interface API
 * @author Parker Lusk <plusk@mit.edu>
 * @date 27 June 2019
 */

#include "esc_interface/esc_interface.h"

#include "utils.h"
#include "pwm.h"

namespace acl {

bool ESCInterface::init()
{
  // Make sure we can connect to PWM device
  if (pwm_init() != PWM_SUCCESS) {
    LOG_ERR("Hardware error: cannot initialize DSP-side peripheral.");
    hw_error_ = true;
    return false;
  }

  // start in (software) disarmed state
  disarm();

  initialized_ = true;
  return true;
}

// ----------------------------------------------------------------------------

ESCInterface::~ESCInterface()
{
  if (initialized_) close();
}

// ----------------------------------------------------------------------------

void ESCInterface::close()
{
  disarm();
  pwm_close();
  initialized_ = false;
}

// ----------------------------------------------------------------------------

bool ESCInterface::arm()
{
  armed_ = true;

  // start at zero throttle
  min_throttle();

  return !hw_error_;
}

// ----------------------------------------------------------------------------

bool ESCInterface::disarm()
{
  // maintain zero throttle to each ESC
  min_throttle();

  armed_ = false;

  return !hw_error_;
}

// ----------------------------------------------------------------------------


bool ESCInterface::update(const uint16_t * pwm, uint16_t len)
{
  // bail if the ESCs are not software armed
  if (!armed_) return false;

  if (len > num_pwm_) {
    LOG_ERR("ESC update error: requested to update more pwms than available.");
    return false;
  }

  // FastRPC call to DSP side
  if (pwm_update(pwm, len) != PWM_SUCCESS) {
    LOG_ERR("Hardware error: cannot set PWM values.");
#if defined(APQ8096)
    LOG_ERR("Hardware error: check connection to PCA9685 board.");
#endif
    hw_error_ = true;
    return false;
  }

  return true;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

bool ESCInterface::min_throttle()
{
  // drive pins to 1000 usec, the min throttle PWM value for ESCs
  uint16_t pwm[num_pwm_];
  for (uint8_t i=0; i<num_pwm_; ++i) pwm[i] = PWM_MIN_PULSE_WIDTH;
  return pwm_update(pwm, num_pwm_);
}

} // ns acl
