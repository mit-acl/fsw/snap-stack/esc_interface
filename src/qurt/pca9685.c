/**
 * @file pca9685.c
 * @brief C API for the PCA9685 I2C to PWM board
 * @author Parker Lusk <plusk@mit.edu>
 * @date 26 June 2019
 */

#include "pca9685.h"

#define MODE1         0x00 /**< Mode Register 1 */
#define PRESCALE      0xFE /**< Prescaler for PWM output frequency */

#define LED0_ON_L     0x06 /**< LED0 output and brightness control byte 0 */
#define LED0_ON_H     0x07 /**< LED0 output and brightness control byte 1 */
#define LED0_OFF_L    0x08 /**< LED0 output and brightness control byte 2 */
#define LED0_OFF_H    0x09 /**< LED0 output and brightness control byte 3 */

#define ALLLED_ON_L   0xFA /**< load all the LEDn_ON registers, byte 0 */
#define ALLLED_ON_H   0xFB /**< load all the LEDn_ON registers, byte 1 */
#define ALLLED_OFF_L  0xFC /**< load all the LEDn_OFF registers, byte 0 */
#define ALLLED_OFF_H  0xFD /**< load all the LEDn_OFF registers, byte 1 */

static int fd_ = -1;
static uint8_t addr_ = 0;

void _swrst();
void _restart();
bool _i2c_slave_config(uint8_t addr);
uint8_t _read8(uint8_t reg);
bool _write8(uint8_t reg, uint8_t data);
bool _write(uint8_t * data, uint8_t len);

// ----------------------------------------------------------------------------

bool pca9685_init(uint8_t i2cdevnum, uint8_t addr)
{

  // TODO: ensure that the appropriate I2C mux gpio is set

  //
  // Open the requested I2C device
  //

  char dev[256];
  snprintf(dev, sizeof(dev), DEV_FS_I2C_DEVICE_TYPE_STRING "%d", i2cdevnum);
  fd_ = open(dev, O_RDWR);

  if (fd_ == -1) {
    LOG_ERR("[pca9685] Opening '%s' failed.", dev);
    return false;
  }

  //
  // Configure I2C device
  //

  addr_ = addr;

  bool ret = _i2c_slave_config(addr);
  if (!ret) return false;

  // restart PWM channels
  _restart();

  LOG_INFO("[pca9685] Successfully opened '%s' "
           "and configured for slave 0x%x.", dev, addr);
  return true;
}

// ----------------------------------------------------------------------------

void pca9685_deinit()
{
  if (fd_ == -1) return;

  // close the file descriptor
  close(fd_);

  // indicate that device is closed
  fd_ = -1;

  LOG_INFO("[pca9685] Deinitialized.");
}

// ----------------------------------------------------------------------------

void pca9685_reset()
{
  // reset chip to power-up state
  _swrst();

  // restart PWM channels
  _restart();
}

// ----------------------------------------------------------------------------

void pca9685_setPWMFrequency(float freq)
{
  // https://github.com/adafruit/Adafruit-PWM-Servo-Driver-Library/issues/11
  freq *= 0.9;

  float prescaleval = 25000000; // 25 MHz
  prescaleval /= 4096;
  prescaleval /= freq;
  prescaleval -= 1;

  uint8_t prescale = (uint8_t)prescaleval;

  uint8_t oldmode = _read8(MODE1);
  uint8_t newmode = (oldmode & 0x7F) | 0x10;  // sleep
  _write8(MODE1, newmode);                    // go to sleep
  _write8(PRESCALE, prescale);                // set the prescaler
  _write8(MODE1, oldmode);                    // restore mode 1 reg
  usleep(5000);
  _write8(MODE1, oldmode | 0xa0);             // turn on auto increment
}

// ----------------------------------------------------------------------------

bool pca9685_setPWM(uint8_t num, uint16_t on, uint16_t off)
{
  uint8_t buf[5] = {
    LED0_ON_L + 4 * num,
    (on & 0x00FF),
    ((on>>8) & 0x00FF),
    (off & 0x00FF),
    ((off>>8) & 0x00FF),
  };

  return _write(buf, 5);
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

void _swrst()
{
  // Configure I2C perhipheral to send a General Call
  _i2c_slave_config(0x00);

  uint8_t data = 0x06;
  _write(&data, 1);
  usleep(1000);

  // Return slave config back to user-specified address
  _i2c_slave_config(addr_);
}

// ----------------------------------------------------------------------------

void _restart()
{
  _write8(MODE1, 0x80);
  usleep(2000);
}

// ----------------------------------------------------------------------------

bool _i2c_slave_config(uint8_t addr)
{
  struct dspal_i2c_ioctl_slave_config slave_config;
  slave_config.slave_address = addr;
  slave_config.bus_frequency_in_khz = 400;
  slave_config.byte_transer_timeout_in_usecs = 9000;

  bool ret = ioctl(fd_, I2C_IOCTL_CONFIG, &slave_config);

  if (ret != 0) {
    LOG_ERR("[pca9685] IOCTL slave 0x%x failed.", addr);
    return false;
  }

  return true;
}

// ----------------------------------------------------------------------------

uint8_t _read8(uint8_t reg)
{
  uint8_t buf = 0;

  struct dspal_i2c_ioctl_combined_write_read ioctl_write_read;
  ioctl_write_read.write_buf     = &reg;
  ioctl_write_read.write_buf_len = 1;
  ioctl_write_read.read_buf      = &buf;
  ioctl_write_read.read_buf_len  = 1;
  uint8_t byte_count = ioctl(fd_, I2C_IOCTL_RDWR, &ioctl_write_read);

  if (byte_count != 1)
    LOG_ERR("[pca9685] Expected 1 byte received, got %d", byte_count);

  return buf;
}

// ----------------------------------------------------------------------------

bool _write8(uint8_t reg, uint8_t data)
{
  uint8_t buf[2] = { reg, data };

  return _write(buf, 2);
}

// ----------------------------------------------------------------------------

bool _write(uint8_t * data, uint8_t len)
{
  uint8_t byte_count = write(fd_, data, len);

  if (byte_count != len) {
    LOG_ERR("[pca9685] Expected %d bytes written, sent %d", len, byte_count);
    return false;
  }

  return true;
}
