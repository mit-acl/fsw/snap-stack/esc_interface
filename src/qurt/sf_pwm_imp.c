/**
 * @file sf_pwm_imp.c
 * @brief DSP implementation of PWM/ESC interface for sf 8074 using pwm sysfs
 * @author Brett Lopez <btlopez@mit.edu>
 * @date 27 July 2017
 */

#include <stdint.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/ioctl.h>

#include <dspal_version.h>
#include <dev_fs_lib_pwm.h>

#include "utils.h"

// Some sf boards may need to use secondary set of PWM pins
// due to damage. See wiki for more info:
// https://gitlab.com/mit-acl/fsw/snap-stack/esc_interface/-/wikis/home
#define USE_SECONDARY 0

#define PWM_MINIMUM_PULSE_WIDTH 1000
#define PWM_PERIOD 2040 // 490 Hz

static struct dspal_pwm pwm_gpio[DEV_FS_PWM_MAX_NUM_SIGNALS];
static struct dspal_pwm_ioctl_signal_definition signal_definition;
static struct dspal_pwm_ioctl_update_buffer *update_buffer;
static struct dspal_pwm *pwm;
static int fd;

int pwm_init(void)
{
    int ret = PWM_SUCCESS;
    struct dspal_version_info version;
    int major_version, minor_version;

    dspal_get_version_info_ext(&version);
    strtok(version.version_string, "-.");
    major_version = atoi(strtok(NULL, "-."));
    minor_version = atoi(strtok(NULL, "-."));

    if (major_version < 1 || (major_version == 1 && minor_version < 1))
    {
        LOG_INFO("Unable to generate PWM signaling, current version: %d.%d, requires version 1.1 or above",
                major_version, minor_version);
        return PWM_ERROR;
    }
    /*
     * Open PWM device
     */
    fd = -1;
    fd = open("/dev/pwm-1", 0);

    if (fd > 0) {
        /*
         * Configure PWM device
         */

#if USE_SECONDARY
        // Define the initial pulse width and number of the GPIO to use for this signal definition.
        pwm_gpio[0].gpio_id = 45;
        pwm_gpio[0].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[1].gpio_id = 46;
        pwm_gpio[1].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[2].gpio_id = 47;
        pwm_gpio[2].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[3].gpio_id = 48;
        pwm_gpio[3].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[4].gpio_id = 27;
        pwm_gpio[4].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[5].gpio_id = 28;
        pwm_gpio[5].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[6].gpio_id = 29;
        pwm_gpio[6].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[7].gpio_id = 30;
        pwm_gpio[7].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
#else
        // Define the initial pulse width and number of the GPIO to use for this signal definition.
        pwm_gpio[0].gpio_id = 4;
        pwm_gpio[0].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[1].gpio_id = 5;
        pwm_gpio[1].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[2].gpio_id = 6;
        pwm_gpio[2].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[3].gpio_id = 7;
        pwm_gpio[3].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[4].gpio_id = 49;
        pwm_gpio[4].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[5].gpio_id = 50;
        pwm_gpio[5].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[6].gpio_id = 51;
        pwm_gpio[6].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
        pwm_gpio[7].gpio_id = 52;
        pwm_gpio[7].pulse_width_in_usecs = PWM_MINIMUM_PULSE_WIDTH;
#endif

        // Describe the overall signal and reference the above array.
        // NOTE: num_gpios needs be even
        signal_definition.num_gpios = 8;
        signal_definition.period_in_usecs = PWM_PERIOD;
        signal_definition.pwm_signal = &pwm_gpio[0];

        // Send the signal definition to the DSP.
        if (ioctl(fd, PWM_IOCTL_SIGNAL_DEFINITION, &signal_definition) != 0) {
            ret = PWM_ERROR;
        }

        // Retrieve the shared buffer which will be used below to update the desired
        // pulse width.
        if (ioctl(fd, PWM_IOCTL_GET_UPDATE_BUFFER, &update_buffer) != 0)
        {
            ret = PWM_ERROR;
        }
        pwm = &update_buffer->pwm_signal[0];
        update_buffer->reserved_1 = 0;

        // Wait for the ESC's to ARM:
        usleep(1000000 * 1); // wait 1 seconds

        LOG_INFO("Initialized");

    } else {
        ret = PWM_ERROR;
    }

    return ret;
}

int pwm_update(const uint16_t* f, uint16_t fLen)
{
    // LOG_INFO("Updating...");
    pwm[0].pulse_width_in_usecs = f[0];
    pwm[1].pulse_width_in_usecs = f[1];
    pwm[2].pulse_width_in_usecs = f[2];
    pwm[3].pulse_width_in_usecs = f[3];
    pwm[4].pulse_width_in_usecs = f[4];
    pwm[5].pulse_width_in_usecs = f[5];
    pwm[6].pulse_width_in_usecs = f[6];
    pwm[7].pulse_width_in_usecs = f[7];

    return PWM_SUCCESS;
}

void pwm_close(void)
{
    /*
     * Close the device ID
     */
    close(fd);
    LOG_INFO("Closing port");
}
