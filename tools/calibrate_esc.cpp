/**
 * @file calibrate_esc.cpp
 * @brief Calibration tool for calibrating ESCs
 * @author Savva Morozov <savva@mit.edu>
 * @date 25 Dec 2019
 */

#include <cmath>
#include <chrono>
#include <csignal>
#include <cstring>
#include <iostream>
#include <sstream>
#include <thread>

#include "esc_interface/esc_interface.h"

// to be set by sighandler
volatile sig_atomic_t stop = 0;
void handle_sigint(int s) { stop = true; }

int main(int argc, char const *argv[])
{
  // install sig handler
  struct sigaction sa;
  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_handler = handle_sigint;
  sa.sa_flags = 0; // not SA_RESTART
  sigaction(SIGINT, &sa, NULL);

  constexpr int NUM_PWMS = 8;
  acl::ESCInterface escs(NUM_PWMS);

  bool success = escs.init();
  if (!success) {
    std::cout << "Could not initialize ESC Interface!!" << std::endl;
    // return -1;
  }
  uint16_t pwm[NUM_PWMS]; // pwm signals to be sent to escs

  std::cout << "REMOVE THE PROPS!" << std::endl;
  std::cout << "Disconnect the ESCs. Press Enter to continue. ";
  std::cin.ignore();
  escs.arm();
  for (int i=0; i<NUM_PWMS; ++i) pwm[i] = 2000; //create max throttle pwm signal.
  escs.update(pwm, NUM_PWMS);

  std::cout << std::endl;
  std::cout << "PWMs are set to max throttle of 2000us. Connect the battery." << std::endl;
  std::cout << "You should hear 3 or 4 beeps indicating battery's cell count (3S, 4S)" << std::endl;
  std::cout << "and an ESC-specific beep squence indicating that max throttle is captured. " << std::endl;
  std::cout << "If not, something is wrong. If yes, press Enter to continue.";
  std::cin.ignore();

  for (int i=0; i<NUM_PWMS; ++i) pwm[i] = 1000; //create min throttle pwm signal.
  escs.update(pwm, NUM_PWMS);
  std::cout << std::endl;
  std::cout << "PWMs are set to min throttle of 1000us." << std::endl;
  std::cout << "You should hear ESC-specific beep sequence indicating that min throttle is captured." << std::endl;
  std::cout << "That's it! Calibration is complete. Test it out with set_pwm tool." << std::endl;
  std::this_thread::sleep_for(std::chrono::milliseconds(5000));

  return 0;
}
